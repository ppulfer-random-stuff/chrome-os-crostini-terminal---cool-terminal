# Make your default Terminal in Chrome OS (Crostini) look awesome

<p align="center"><img src="https://i.imgur.com/ZkaDRLu.png" /></p>

Difficulty: EASY

Requirements
* Linux enabled in Chrome OS
* Any prefered editor
* NeoFetch for aditional awesomeness

Steps
1. Open your Linux Terminal
2. Install NeoFetch with the following command: `sudo apt-get install neofetch`
3. Open .bashrc with your prefered editor (example: `nano .bashrc`)
4. Add the following code (copy/paste) to the END of your .bashrc document: [CLICK HERE](https://raw.githubusercontent.com/ChrisTitusTech/scripts/master/fancy-bash-promt.sh)
5. Add the following code (copy/paste) to the END of your .bashrc document and change the `YOURNAME` part to anything you'd like: 
```
echo
neofetch
echo
echo -e "\e[44m Welcome YOURNAME!"
echo
```
6. Save it and restart your Terminal